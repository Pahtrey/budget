<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Envelope extends Model
{
    protected $fillable = ['name', 'color', 'active', 'sort'];
    protected $table = 'envelope';

    public function setProperties(Request $request)
    {
        $this->name = $request->name;
        $this->color = $request->color;
        $this->active = (int) $request->active;
        $this->sort = (int) $request->sort;
    }

    public static function getList()
    {
        $envelopes = Envelope::orderBy('sort', 'asc')->get(); //where('active', 1)->
        return $envelopes;
    }

    public static function getActiveList()
    {
        $envelopes = Envelope::where('active', 1)->orderBy('sort', 'asc')->get();
        return $envelopes;
    }

    public function expenditures()
    {
        return $this->hasMany(Rate::class);
    }
}
