<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnvelopeRate extends Model
{
    protected $fillable = ['amount', 'budget_id', 'envelope_id'];
    protected $table = 'budget_dependence';

    public static function createDependenceByBudgetId($budgetId)
    {
        if ($budgetId)
        {
            $envelopes = Envelope::getActiveList();

            foreach ($envelopes as $envelope)
            {
                $budgetDependence = new EnvelopeRate;
                $budgetDependence->create([
                    'budget_id' => $budgetId,
                    'envelope_id' => $envelope->id,
                    'amount' => 0
                ]);
            }
        }
    }

    public static function getDependenceByBudgetId($budgetId)
    {
        $dependence = [];

        if ($budgetId)
        {
            $dependence = EnvelopeRate::where('budget_id', $budgetId)->get();
        }

        return $dependence;
    }



    public function budget()
    {
        return $this->belongsTo(Budget::class);
    }

    public function expenditures()
    {
        return $this->hasMany(Rate::class);
    }
}