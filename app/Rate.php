<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = ['caption', 'spend', 'budget_dependence_id'];
    protected $table = 'rate';

    public static function getRateSum($rates)
    {
        $rateSum = 0;

        foreach ($rates as $rate)
        {
            $rateSum += $rate->spend;
        }

        return $rateSum;
    }

    public function dependence()
    {
        return $this->belongsTo(EnvelopeRate::class);
    }

    public function envelope()
    {
        return $this->belongsTo(Envelope::class);
    }
}
