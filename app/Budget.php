<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    protected $fillable = ['income'];
    protected $table = 'budget';

    public function getRateByAllEnvelopes()
    {
        $rate = 0;
        $envelopeRates = $this->dependence()->get();

        if ($envelopeRates)
        {
            $rate = $this->sumRates($envelopeRates);
        }

        return $rate;
    }

    public function getRateWithoutOneEnvelope($id)
    {
        $rate = 0;
        $envelopeRates = $this->dependence()->where('envelope_id', '!=', $id)->get();

        if ($envelopeRates)
        {
            $rate = $this->sumRates($envelopeRates);
        }

        return $rate;
    }

    private function sumRates($envelopeRates)
    {
        $result = 0;

        foreach ($envelopeRates as $envelopeRate)
        {
            $result += $envelopeRate->amount;
        }

        return $result;
    }

    public static function getBudgetBalance($budgetId, $dependenceId)
    {
        $budget = Budget::findOrFail($budgetId);
        $balance = $budget->income - $budget->getRateWithoutOneEnvelope($dependenceId);

        return $balance;
    }

    public static function getCurrentMonthBudget($budgets)
    {
        $currentBudget = null;

        foreach ($budgets as $budget)
        {
            if (strtotime(date("Y-m", strtotime($budget->created_at))) == strtotime(date("Y-m")))
            {
                $currentBudget = $budget;
            }
        }

        return $currentBudget;
    }

    public function getRates()
    {
        $budgetRates = 0;
        $budgetDependence = EnvelopeRate::getDependenceByBudgetId($this->id);

        foreach ($budgetDependence as $dependence)
        {
            $rates = Rate::where('budget_dependence_id', $dependence->id)->get();
            $budgetRates += Rate::getRateSum($rates);
        }

        return $budgetRates;
    }

    public function generateBudgetEnvelopesArray()
    {
        $budgetData = [];
        $budgetDependence = EnvelopeRate::getDependenceByBudgetId($this->id);
        $envelopes = Envelope::getList();

        foreach ($budgetDependence as $dependence)
        {
            $budgetData[$dependence->envelope_id]['amount'] = ($dependence->amount)? $dependence->amount: 0;
            $budgetData[$dependence->envelope_id]['dependenceId'] = $dependence->id;

            $budgetData[$dependence->envelope_id]['rates'] = [];

            if ($dependence->envelope_id)
            {
                $rates = Rate::where('budget_dependence_id', $dependence->id)->orderBy('created_at', 'desc')->get();
                $budgetData[$dependence->envelope_id]['rates'] = $rates;
                $budgetData[$dependence->envelope_id]['spend'] = Rate::getRateSum($rates);
            }
        }

        foreach ($envelopes as $envelope)
        {
            if (isset($budgetData[$envelope->id]['dependenceId']))
            {
                $budgetData[$envelope->id]['name'] = $envelope->name;
                $budgetData[$envelope->id]['color'] = $envelope->color;
            }
        }

        return $budgetData;
    }

    public static function getPercentSpend($spend, $income)
    {
        $percent = 0;

        if ($income > 0)
        {
            $percent = ceil($spend * 100 / $income);
        }

        return $percent;
    }

    public function showControls()
    {
        $result = true;

        if (strtotime(date("Y-m", strtotime($this->created_at))) != strtotime(date("Y-m")))
        {
            $result = false;
        }

        return $result;
    }

    public static function convertDateFormat($date)
    {
        $convertedDate = '';
        setlocale (LC_TIME, 'ru_RU', 'Rus');

        if ($date)
        {
            $convertedDate = iconv('CP1251', 'UTF-8', strftime("%B %Y", strtotime($date)));
        }

        return $convertedDate;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function dependence()
    {
        return $this->hasMany(EnvelopeRate::class);
    }
}