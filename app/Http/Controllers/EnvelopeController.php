<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Envelope;

class EnvelopeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $envelopes = Envelope::orderBy('created_at', 'asc')->get();

        return view('envelope.index', [
            'envelopes' => $envelopes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('envelope.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'color' => 'required|max:100',
            'sort' => 'max:100'
        ]);

        $envelope = new Envelope;

        $envelope->setProperties($request);

        $envelope->save();

        return redirect('/envelope');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $envelope = Envelope::findOrFail($id);

        return view('envelope.show', [
            'envelope' => $envelope,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $envelope = Envelope::findOrFail($id);

        return view('envelope.edit', [
            'envelope' => $envelope
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'color' => 'required|max:100',
            'sort' => 'max:100'
        ]);

        $envelope = Envelope::findOrFail($id);

        $envelope->setProperties($request);

        $envelope->update();

        if ($request->action == "apply")
        {
            return view('envelope.edit', [
                'envelope' => $envelope
            ]);
        }
        else
        {
            return redirect('/envelope');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
