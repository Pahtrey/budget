<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Envelope;
use App\Budget;
use App\EnvelopeRate;
use App\Rate;

class BudgetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $budgets = $request->user()->budgets()->get();
        $currentBudget = Budget::getCurrentMonthBudget($budgets);
        $currentBudgetRate = ($currentBudget)? $currentBudget->getRateByAllEnvelopes(): 0;
        $budgetSumRates = ($currentBudget)? $currentBudget->getRates(): 0;
        $budgetDate = ($currentBudget)? Budget::convertDateFormat($currentBudget->created_at): 0;
        $percentSpend = ($currentBudget)? Budget::getPercentSpend($budgetSumRates, $currentBudget->income): 0;

        return view('budget.index', [
            'currentBudget' => $currentBudget,
            'currentBudgetRate' => $currentBudgetRate,
            'budgets' => $budgets,
            'date' => $budgetDate,
            'rate' => $budgetSumRates,
            'percent' => $percentSpend
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('budget.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'income' => 'required|max:11'
        ]);

        $budget = $request->user()->budgets()->create([
            'income' => $request->income
        ]);

        EnvelopeRate::createDependenceByBudgetId($budget->id);

        return redirect('/budget');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $budget = $request->user()->budgets()->findOrFail($id);
        $budgetDate = Budget::convertDateFormat($budget->created_at);
        $budgetRate = $budget->getRateByAllEnvelopes();
        $showControls = $budget->showControls();
        $envelopes = $budget->generateBudgetEnvelopesArray();
        $budgetSumRates = $budget->getRates();

        return view('budget.show', [
            'budget' => $budget,
            'budgetRate' => $budgetRate,
            'rate' => $budgetSumRates,
            'envelopes' => $envelopes,
            'date' => $budgetDate,
            'showControls' => $showControls
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
