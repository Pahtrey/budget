<?php

namespace App\Http\Controllers;

use App\Budget;
use App\EnvelopeRate;
use Illuminate\Http\Request;
use App\Http\Requests;

class EnvelopeRateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return $request->budget_id;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $budgetId
     * @param  int  $envelopeId
     * @return \Illuminate\Http\Response
     */
    public function edit($budgetId, $envelopeId)
    {
        $envelopeRate = Budget::findOrfail($budgetId)->dependence()->where('envelope_id', $envelopeId)->first();

        return view('envelope_rate.edit', [
            'budgetId' => $budgetId,
            'envelopeId' => $envelopeId,
            'envelopeRate' => $envelopeRate
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $budgetId
     * @param  int  $dependenceId
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $budgetId, $dependenceId, $id)
    {
        $this->validate($request, [
            'amount' => 'required|max:11'
        ]);

        $envelopeRate = EnvelopeRate::findOrfail($id);
        $envelopeRate->amount = $request->amount;

        $budgetDiffBalance =  Budget::getBudgetBalance($budgetId, $dependenceId) - $envelopeRate->amount;

        if ($budgetDiffBalance >= 0)
        {
            $envelopeRate->update();

            return redirect('/budget/' . $budgetId);
        }
        else
        {
            return redirect('/budget/' . $budgetId . '/envelope_rate/' . $dependenceId . '/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
