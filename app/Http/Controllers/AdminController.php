<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use App\Budget;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countUsers = count(User::all());
        $countBudgets = count(Budget::all());

        return view('admin.index', [
            'countUsers' => $countUsers,
            'countBudgets' => $countBudgets
        ]);
    }

    public function showBudgets()
    {
        return view('admin.index');
    }
}
