<?php

Route::get('/', function () {
    return view('index');
});

Route::get('/about', function () {
    return 'about page';
});

Route::auth();

Route::group(['middleware' => ['admin']], function () {

    Route::get('/admin', 'AdminController@index');

    //Route::resource('budget', 'BudgetController');
    Route::resource('envelope', 'EnvelopeController');
    Route::resource('user', 'UserController');
});

Route::group(['middleware' => ['web']], function () {


    //Route::get('/home', 'HomeController@index');
    //Route::get('/admin', 'AdminController@index');

    Route::get('budget/{budget_id}/envelope_rate/{id}/edit', 'EnvelopeRateController@edit');
    Route::put('budget/{budget_id}/envelope_rate/{envelope_id}/{id}', 'EnvelopeRateController@update');
    Route::get('budget/{budget_id}/dependence/{dependence_id}/rate/create', 'RateController@create');
    Route::post('budget/rate/store', 'RateController@store');
    Route::delete('budget/rate/{id}', 'RateController@destroy');


    Route::resource('budget', 'BudgetController');


    //Route::resource('envelope_rate', 'EnvelopeRateController');
});
