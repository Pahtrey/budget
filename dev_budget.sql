-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 01 2016 г., 07:46
-- Версия сервера: 5.5.23
-- Версия PHP: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dev.budget`
--

-- --------------------------------------------------------

--
-- Структура таблицы `budget`
--

CREATE TABLE `budget` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `income` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `budget`
--

INSERT INTO `budget` (`id`, `user_id`, `income`, `created_at`, `updated_at`) VALUES
(1, 2, 15000, '2016-06-01 05:00:00', '2016-06-01 05:00:00'),
(2, 2, 10000, '2016-07-01 03:45:08', '2016-07-01 03:45:08'),
(3, 3, 20000, '2016-07-01 04:25:03', '2016-07-01 04:25:03');

-- --------------------------------------------------------

--
-- Структура таблицы `budget_dependence`
--

CREATE TABLE `budget_dependence` (
  `id` int(10) UNSIGNED NOT NULL,
  `budget_id` int(11) NOT NULL,
  `envelope_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `budget_dependence`
--

INSERT INTO `budget_dependence` (`id`, `budget_id`, `envelope_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3000, '2016-07-01 03:27:19', '2016-07-01 03:27:37'),
(2, 1, 2, 3000, '2016-07-01 03:27:19', '2016-07-01 03:27:44'),
(3, 1, 3, 4500, '2016-07-01 03:27:19', '2016-07-01 03:29:50'),
(4, 1, 4, 0, '2016-07-01 03:27:19', '2016-07-01 03:27:19'),
(5, 1, 5, 2500, '2016-07-01 03:27:19', '2016-07-01 03:28:57'),
(6, 1, 6, 1000, '2016-07-01 03:27:19', '2016-07-01 03:28:42'),
(7, 1, 7, 1000, '2016-07-01 03:27:19', '2016-07-01 03:28:34'),
(8, 2, 1, 1500, '2016-07-01 03:45:08', '2016-07-01 03:45:18'),
(9, 2, 2, 3000, '2016-07-01 03:45:08', '2016-07-01 03:45:23'),
(10, 2, 3, 1000, '2016-07-01 03:45:08', '2016-07-01 03:45:31'),
(11, 2, 4, 0, '2016-07-01 03:45:08', '2016-07-01 03:45:08'),
(12, 2, 5, 2500, '2016-07-01 03:45:08', '2016-07-01 03:45:49'),
(13, 2, 6, 1000, '2016-07-01 03:45:08', '2016-07-01 03:45:36'),
(14, 2, 7, 1000, '2016-07-01 03:45:08', '2016-07-01 03:45:41'),
(15, 3, 1, 5000, '2016-07-01 04:25:03', '2016-07-01 04:25:21'),
(16, 3, 2, 3000, '2016-07-01 04:25:03', '2016-07-01 04:25:27'),
(17, 3, 3, 4000, '2016-07-01 04:25:03', '2016-07-01 04:26:00'),
(18, 3, 4, 0, '2016-07-01 04:25:03', '2016-07-01 04:25:03'),
(19, 3, 5, 2500, '2016-07-01 04:25:03', '2016-07-01 04:26:08'),
(20, 3, 6, 0, '2016-07-01 04:25:03', '2016-07-01 04:25:03'),
(21, 3, 7, 1000, '2016-07-01 04:25:03', '2016-07-01 04:25:41');

-- --------------------------------------------------------

--
-- Структура таблицы `envelope`
--

CREATE TABLE `envelope` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `sort` int(100) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `envelope`
--

INSERT INTO `envelope` (`id`, `name`, `color`, `active`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Питание', '#24c285', 1, 10, '2016-06-30 21:25:03', '2016-07-01 04:23:36'),
(2, 'ЖКХ', '#9c4af0', 1, 20, '2016-07-01 03:22:44', '2016-07-01 03:42:58'),
(3, 'Покупки', '#dedb8d', 1, 30, '2016-07-01 03:23:35', '2016-07-01 03:23:35'),
(4, 'Дети', '#92d3dd', 1, 40, '2016-07-01 03:24:04', '2016-07-01 03:24:04'),
(5, 'Развлечения', '#f1aa6b', 1, 50, '2016-07-01 03:24:34', '2016-07-01 03:24:34'),
(6, 'Накопления', '#f68989', 1, 60, '2016-07-01 03:25:07', '2016-07-01 03:25:07'),
(7, 'Черный день', '#6b6b6b', 1, 70, '2016-07-01 03:25:33', '2016-07-01 03:25:33');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_16_163324_create_budgets_table', 2),
('2016_06_20_151234_create_envelopes_table', 3),
('2016_06_20_161121_create_expenditures_table', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `rate`
--

CREATE TABLE `rate` (
  `id` int(10) UNSIGNED NOT NULL,
  `caption` varchar(255) NOT NULL,
  `budget_dependence_id` int(11) NOT NULL,
  `spend` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `rate`
--

INSERT INTO `rate` (`id`, `caption`, `budget_dependence_id`, `spend`, `created_at`, `updated_at`) VALUES
(1, 'Хлеб', 1, 25, '2016-07-01 03:30:29', '2016-07-01 03:30:29'),
(2, 'Молоко', 1, 50, '2016-07-01 03:30:39', '2016-07-01 03:30:39'),
(3, 'Электричество', 2, 300, '2016-07-01 03:31:01', '2016-07-01 03:31:01'),
(4, 'Кроссовки', 3, 2000, '2016-07-01 03:31:27', '2016-07-01 03:31:27'),
(5, 'Кино', 5, 200, '2016-07-01 03:31:40', '2016-07-01 03:31:40'),
(6, 'Пельмени', 1, 150, '2016-07-01 03:32:21', '2016-07-01 03:32:21'),
(7, 'Шашлык', 1, 300, '2016-07-01 03:32:38', '2016-07-01 03:32:38'),
(8, 'Шоколад', 1, 100, '2016-07-01 03:32:48', '2016-07-01 03:32:48'),
(9, 'Футболка', 3, 1000, '2016-07-01 03:38:32', '2016-07-01 03:38:32'),
(10, 'Поход', 5, 1000, '2016-07-01 03:39:21', '2016-07-01 03:39:21'),
(11, 'Кафе', 5, 500, '2016-07-01 03:39:31', '2016-07-01 03:39:31'),
(12, 'Лобстер', 1, 1500, '2016-07-01 03:39:51', '2016-07-01 03:39:51'),
(13, 'Аквапарк', 12, 2000, '2016-07-01 03:49:31', '2016-07-01 03:49:31'),
(14, 'Вода', 9, 400, '2016-07-01 03:49:49', '2016-07-01 03:49:49'),
(15, 'Электричество', 9, 300, '2016-07-01 03:49:56', '2016-07-01 03:49:56'),
(16, 'Рюкзак', 10, 1000, '2016-07-01 03:50:07', '2016-07-01 03:50:07'),
(17, 'Доширак', 8, 500, '2016-07-01 03:50:22', '2016-07-01 03:50:22'),
(18, 'Ремонт авто', 13, 1000, '2016-07-01 03:50:54', '2016-07-01 03:50:54'),
(19, 'Ремонт авто', 14, 500, '2016-07-01 03:51:02', '2016-07-01 03:51:02'),
(20, 'Кварплата', 9, 1000, '2016-07-01 03:51:21', '2016-07-01 03:51:21'),
(21, 'Рюкзак', 17, 1000, '2016-07-01 04:26:35', '2016-07-01 04:26:35'),
(22, 'Аквапарк', 19, 1500, '2016-07-01 04:26:46', '2016-07-01 04:26:46'),
(24, 'Штаны', 17, 2000, '2016-07-01 04:27:26', '2016-07-01 04:27:26'),
(25, 'Пельмени', 15, 1000, '2016-07-01 04:27:36', '2016-07-01 04:27:36'),
(26, 'Кварплата', 16, 1500, '2016-07-01 04:28:01', '2016-07-01 04:28:01'),
(27, 'Газировка', 15, 100, '2016-07-01 04:28:15', '2016-07-01 04:28:15');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `is_admin`) VALUES
(1, 'Павел', 'admin@admin.ru', '$2y$10$Va5aVXVqYYq/MzIiu98ZJ.a0NO59AG0KumjMaBQo.vgMQ7Iqi8y3C', 'PKc8OxqC4uidMOe1sGvx8dbRz0aORez1fLFn1VrPxCA4hInMYtaARVgbX7MP', '2016-06-15 13:20:37', '2016-07-01 04:23:43', 1),
(2, 'Алексей', 'user@user.ru', '$2y$10$66AGAnBfEZGVuiCNKB9oCuKjjUZt2yR2O21BCyU3GiZp9sZ/gA3fu', 'e8ZcH4q0q4Sgnd1lnRFWjhNMzuzxrLA1ZExb2JQsv0e0wfk6YcjbbugsxGRa', '2016-06-15 14:25:19', '2016-07-01 04:42:18', 0),
(3, 'Владимир', 'user2@user.ru', '$2y$10$N/rJDoEAcktsgxeoFi6QJuCKY1rKaBMK5TLy2Sh8UKG5dVrJa8gne', 'TBLufGTdoQgN94VteXOrLAbC9fvu6EmNQ0msjaSwQ6jAcBgrGHDH1IEFDEVM', '2016-06-27 10:13:52', '2016-07-01 04:28:57', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `budget`
--
ALTER TABLE `budget`
  ADD PRIMARY KEY (`id`),
  ADD KEY `budgets_user_id_index` (`user_id`);

--
-- Индексы таблицы `budget_dependence`
--
ALTER TABLE `budget_dependence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `budget_id` (`budget_id`),
  ADD KEY `envelope_id` (`envelope_id`);

--
-- Индексы таблицы `envelope`
--
ALTER TABLE `envelope`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenditure_envelope_id_index` (`budget_dependence_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `budget`
--
ALTER TABLE `budget`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `budget_dependence`
--
ALTER TABLE `budget_dependence`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `envelope`
--
ALTER TABLE `envelope`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
