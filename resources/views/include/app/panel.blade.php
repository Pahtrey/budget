<div class="user">
    <span class="avatar"><i class="fa fa-user"></i></span>
    <span class="name">{{ Auth::user()->name }}</span>
</div>
<ul class="nav nav-pills nav-stacked">
@if (Auth::user()->is_admin)
    <li><a href="{{ url('/admin') }}">Панель</a></li>
    <li><a href="{{ url('/envelope') }}">Конверты</a></li>
    <li><a href="{{ url('/user') }}">Пользователи</a></li>
@else
    <li><a href="{{ url('/budget') }}">Бюджеты</a></li>
@endif
</ul>