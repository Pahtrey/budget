<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Budget planner</title>

    @include('include.head')

</head>
<body id="app-layout" class="page-main">

    @include('include.header')

    <div class="content content-main">
    @yield('content')
    </div>

    @include('include.footer')
</body>
</html>
