@extends('layouts.app')

@section('content')

<div class="panel panel-default col-sm-offset-2 col-sm-8">
    <div class="panel-heading">
        Создание конверта
    </div>
    <div class="panel-body">
        <form action="{{ url('/envelope') }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="envelope-name" class="col-sm-4 control-label">Конверт</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="envelope-name" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="envelope-color" class="col-sm-4 control-label">Цвет конверта</label>
                <div class="col-sm-6">
                    <input type="text" name="color" id="envelope-color" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="envelope-active" class="col-sm-4 control-label">Активен</label>
                <div class="col-sm-6">
                    <input type="checkbox" name="active" id="envelope-active" value="1">
                </div>
            </div>

            <div class="form-group">
                <label for="envelope-sort" class="col-sm-4 control-label">Сортировка</label>
                <div class="col-sm-6">
                    <input type="text" name="sort" id="envelope-sort" class="form-control" value="100">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Создать
                    </button>
                    <a href="{{ url('/envelope') }}" class="btn btn-default">Отменить</a>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection