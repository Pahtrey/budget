@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Список конвертов</div>
            <div class="panel-body">
                <a class="btn btn-success pull-right" href="{{ url('/envelope/create/') }}">Создать конверт</a>
                @if (count($envelopes) > 0)
                <table class="table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Конверт</th>
                        <th>Цвет</th>
                        <th>Активность</th>
                        <th>Сортировка</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($envelopes as $envelope)
                    <tr>
                        <th scope="row">{{ $envelope->id }}</th>
                        <td><a href="{{ url('/envelope/' . $envelope->id) }}">{{ $envelope->name }}</a></td>
                        <td>{{ $envelope->color }}</td>
                        <td><span class="fa {{ ($envelope->active)? 'fa-eye': 'fa-eye-slash' }}"></span></td>
                        <td>{{ $envelope->sort }}</td>
                        <td><a href="{{ url('/envelope/' . $envelope->id. '/edit/') }}" class="btn btn-default btn-sm pull-right"><span class="fa fa-pencil"></span></a></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection