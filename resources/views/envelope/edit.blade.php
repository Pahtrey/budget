@extends('layouts.app')

@section('content')

<div class="panel panel-default col-sm-offset-2 col-sm-8">
    <div class="panel-heading">
        Редактирование конверта
    </div>
    <div class="panel-body">
        @include('include.common.errors')
        <form action="{{ url('/envelope/' . $envelope->id) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                <label for="envelope-name" class="col-sm-4 control-label">Конверт</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="envelope-name" class="form-control" value="{{ $envelope->name }}">
                </div>
            </div>
            <div class="form-group">
                <label for="envelope-color" class="col-sm-4 control-label">Цвет конверта</label>
                <div class="col-sm-6">
                    <input type="text" name="color" id="envelope-color" class="form-control" value="{{ $envelope->color }}">
                </div>
            </div>

            <div class="form-group">
                <label for="envelope-active" class="col-sm-4 control-label">Активен</label>
                <div class="col-sm-6">
                    <input type="checkbox" name="active" id="envelope-active" value="{{ $envelope->active }}" {{ ($envelope->active)? 'checked': '' }}>
                </div>
            </div>

            <div class="form-group">
                <label for="envelope-sort" class="col-sm-4 control-label">Сортировка</label>
                <div class="col-sm-6">
                    <input type="text" name="sort" id="envelope-sort" class="form-control" value="{{ $envelope->sort }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-default" name="action" value="submit">Сохранить</button>
                    <button type="submit" class="btn btn-default" name="action" value="apply">Применить</button>
                    <a href="{{ url('/envelope/') }}" class="btn btn-default">Отменить</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection