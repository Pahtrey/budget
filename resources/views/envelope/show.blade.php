@extends('layouts.app')

@section('content')

<div class="panel panel-default col-sm-offset-2 col-sm-8">
    <div class="panel-heading">
        Просмотр конверта
    </div>
    <div class="panel-body">
        <table class="table table-borderless">
            <tbody>
            <tr>
                <td>Название</td>
                <td>{{ $envelope->name }}</td>
            </tr>
            <tr>
                <td>Цвет</td>
                <td>{{ $envelope->color }}</td>
            </tr>
            <tr>
                <td>Активно</td>
                <td>{{ $envelope->active }}</td>
            </tr>
            </tbody>
        </table>
        <div class="text-center">
            <a href="{{ url('/envelope') }}" class="btn btn-default">Назад</a>
        </div>
    </div>
</div>

@endsection