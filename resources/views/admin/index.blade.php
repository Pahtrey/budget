@extends('layouts.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Панель администрирования
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-3">
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <td>Пользователей в системе</td>
                        <td><span class="pull-right">{{ $countUsers }}</span></td>
                    </tr>
                    <tr>
                        <td>Бюджетов создано</td>
                        <td><span class="pull-right">{{ $countBudgets }}</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
