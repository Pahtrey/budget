@extends('layouts.app')

@section('content')

<div class="panel panel-default col-sm-offset-2 col-sm-8">
    <div class="panel-heading">
        Просмотр пользователя
    </div>
    <div class="panel-body">
        <table class="table table-borderless">
            <tbody>
            <tr>
                <td>Имя</td>
                <td>{{ $user->name }}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <td>Дата рагистрации</td>
                <td>{{ $user->created_at }}</td>
            </tr>
            <tr>
                <td>Администратор</td>
                <td>{{ $user->is_admin }}</td>
            </tr>
            </tbody>
        </table>
        <div class="text-center">
            <a href="{{ url('/user') }}" class="btn btn-default">Назад</a>
        </div>
    </div>
</div>

@endsection