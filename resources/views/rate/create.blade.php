@extends('layouts.app')

@section('content')

<div class="panel panel-default col-sm-offset-2 col-sm-8">
    <div class="panel-heading">
        Взять из конверта
    </div>
    <div class="panel-body">
        <form action="{{ url('/budget/rate/store') }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <input type="hidden" name="dependence_id" value="{{ $dependenceId }}">
            <input type="hidden" name="budget_id" value="{{ $budgetId }}">

            <div class="form-group">
                <label for="rate-caption" class="col-sm-4 control-label">Описание</label>
                <div class="col-sm-6">
                    <input type="text" name="caption" id="rate-caption" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="rate-spend" class="col-sm-4 control-label">Сумма</label>
                <div class="col-sm-6">
                    <input type="text" name="spend" id="rate-spend" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Создать
                    </button>
                    <a href="{{ url('/budget/'. $budgetId) }}" class="btn btn-default">Отменить</a>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection