@extends('layouts.app')

@section('content')

<div class="panel panel-default col-sm-offset-2 col-sm-8">
    <div class="panel-heading">
        Положить в конверт
    </div>
    <div class="panel-body">
        <form action="{{ url('/budget/' . $budgetId . '/envelope_rate/' . $envelopeId . '/' . $envelopeRate->id) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">

            <div class="form-group">
                <label for="envelope_rate-amount" class="col-sm-4 control-label">Сумма</label>
                <div class="col-sm-6">
                    <input type="text" name="amount" id="envelope_rate-amount" class="form-control" value="{{ $envelopeRate->amount }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Сохранить
                    </button>
                    <a href="{{ url('/budget/' . $budgetId) }}" class="btn btn-default">Отменить</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection