@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Управление бюджетом</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-5">
                        <table class="table table-borderless">
                            <tbody>
                            <tr>
                                <td colspan="2">{{ $date }}</td>
                            </tr>
                            <tr>
                                <td>Доход</td>
                                <td><span class="pull-right">{{ $budget->income }} руб.</span></td>
                            </tr>
                            <tr>
                                <td>Распределено</td>
                                <td><span class="pull-right">{{ $budgetRate }} руб.</span></td>
                            </tr>
                            <tr>
                                <td>Расход</td>
                                <td><span class="pull-right">{{ $rate }} руб.</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @if (count($envelopes) > 0)
            <div class="row block-envelope">
            @foreach ($envelopes as $envelopeId => $envelope)
                <div class="col-md-3">
                    <div class="panel panel-default panel-envelope">
                        <div class="panel-heading">
                            {{ $envelope['name'] }}
                            @if ($showControls)
                            <div class="pull-right envelope">
                                <a href="{{ url('/budget/' . $budget->id . '/envelope_rate/' . $envelopeId . '/edit' ) }}" class="btn btn-default"  style="background: {{ $envelope['color'] }};">
                                    <i class="fa fa-envelope-o icon-envelope"></i>
                                </a>
                            </div>
                            @endif
                        </div>
                        <div class="panel-body">
                            <span class="pull-left">{{ $envelope['amount'] }} <i class="fa fa-level-up"></i></span>
                            <span class="pull-right"><i class="fa fa-level-down"></i> {{ $envelope['spend'] }}</span>
                        </div>
                        @if (count($envelope['rates']) > 0)
                            <ul class="list-group collapse" id="list-rates-{{ $envelopeId }}">
                            @foreach ($envelope['rates'] as $rate)
                                <li class="list-group-item">
                                    {{ $rate->caption }} - {{ $rate->spend }}
                                    @if ($showControls)
                                    <div class="pull-right">
                                        <form action="{{ url('/budget/rate/' . $rate->id ) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="budgetId" value="{{ $budget->id }}">
                                            <button type="submit" class="btn btn-danger btn-xs btn-delete">
                                                <i class="fa fa-btn fa-close"></i>
                                            </button>
                                        </form>
                                    </div>
                                    @endif
                                </li>
                            @endforeach
                        @endif
                        </ul>
                        @if ($showControls && $envelope['amount'] > 0)
                        <div class="panel-footer clearfix">
                            <a href="{{ url('/budget/' . $budget->id . '/dependence/' . $envelope['dependenceId'] . '/rate/create' ) }}" class="btn btn-default pull-left"><i class="fa fa-plus-circle"></i></a>
                            <button type="button" class="btn btn-default collapsed pull-right" data-toggle="collapse" data-target="#list-rates-{{ $envelopeId }}"><i class="fa fa-arrows-v"></i></button>
                        </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
        @endif
    </div>
</div>

@endsection