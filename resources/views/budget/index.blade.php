@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default budget-intro">
            <div class="panel-heading">Информация по текущему бюджету</div>
            <div class="panel-body">
                @if (count($currentBudget) > 0)
                    <h1><a href="{{ url('/budget/' . $currentBudget->id) }}">{{ $date }}</a></h1>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge">{{ $currentBudget->income }} руб.</span>
                            Доход
                        </li>
                        <li class="list-group-item">
                            <span class="badge"> {{ $rate }} руб.</span>
                            Расход
                        </li>
                    </ul>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{{ $rate }}" aria-valuemin="0" aria-valuemax="{{ $currentBudget->income }}" style="width: {{ $percent }}%;">
                            {{ $percent }}%
                        </div>
                    </div>
                @else
                    <p>Бюджет на текущий месяц не создан</p>
                    <a class="btn btn-default" href="{{ url('/budget/create') }}"><i class="fa fa-plus"></i> Создать</a>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Список бюджетов</div>
            @if (count($budgets) > 0)
            <div class="list-group">
                @foreach ($budgets as $budget)
                <a href="{{ url('/budget/' . $budget->id) }}" class="list-group-item">{{ $budget->created_at->format('m.Y') }}</a>
                @endforeach
            </div>
            @else
            <div class="panel-body">
                Список пуст
            </div>
            @endif
        </div>
    </div>
</div>

@endsection
