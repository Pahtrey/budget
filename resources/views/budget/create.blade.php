@extends('layouts.app')

@section('content')

<div class="panel panel-default col-sm-offset-2 col-sm-8">
    <div class="panel-heading">
        Создание конверта
    </div>
    <div class="panel-body">
        <form action="{{ url('/budget') }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="budget-name" class="col-sm-3 control-label">Доход</label>

                <div class="col-sm-6">
                    <input type="text" name="income" id="budget-name" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Создать
                    </button>
                    <a href="{{ url('/budget') }}" class="btn btn-default">Отменить</a>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection